#pragma once
#include <vector>
#include <cmath>
#include <stdexcept>

//функция сравнения чисел double
inline bool isEqual(double x, double y);

//функция решения квадратного уравнения
std::vector<double> QuadraticEquation(double A_a, double A_b, double A_c);
