#include "QuadraticEquation.h"
#define EPS 0.000001

inline bool isEqual(double x, double y)
{
   return fabs(x - y) <= EPS;
}

std::vector<double> QuadraticEquation(double A_a, double A_b, double A_c){
    if (isEqual(A_a, 0.0)){
        throw std::invalid_argument("Argument a is zero.");
    }

    std::vector<double> res;
    double d = 0, x1 = 0, x2 = 0;
  
    d = A_b * A_b - 4 * A_a * A_c; // Рассчитываем дискриминант
    
    if (d > EPS) // Условие при дискриминанте больше нуля
    {
        x1 = ((-A_b) - sqrt(d)) / (2 * A_a);
        x2 = ((-A_b) + sqrt(d)) / (2 * A_a);
        if(x1<x2){
            res.push_back(x1);
            res.push_back(x2);
        }else{
            res.push_back(x2);
            res.push_back(x1);
        }        
        return res;
    }
    
    if (isEqual(d, 0)) // Условие для дискриминанта равного нулю
    {
        x1 = -(A_b / (2 * A_a));
        res.push_back(x1);
        res.push_back(x1);
        return res;
    }

    // Условие при дискриминанте меньше нуля
	return res;
}
